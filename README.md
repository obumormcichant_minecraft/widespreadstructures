# Widespread Structures

This Minecraft data pack overrides most structure placement settings,
placing them from 2–4 times farther apart than normal.  It’s meant for
use in worlds with large continent sizes and/or large biomes where
players will be exploring farther out, to avoid their repetitiveness.

The current build should be compatible with Minecraft Java edition
1.20.2 – 1.21.4 (data pack formats 18–61).

## Specific Placement Changes

| Structure or Feature | Spread         | Compared to Normal |
| -------------------- | -------------- | ------------------ |
| Abandoned Mineshaft  | 0.1% of chunks | ¼ as frequent      |
| Amethyst Geode       | 1.5 km         | 4× farther apart   |
| Ancient City         | 770 m          | 2× farther apart   |
| Buried Treasure      | 64 m           | 4× farther apart   |
| Desert Pyramid       | 1 km           | 2× farther apart   |
| End City             | 640 m          | 2× farther apart   |
| Igloo                | 1 km           | 2× farther apart   |
| Jungle Temple        | 1 km           | 2× farther apart   |
| Lava Lake (surface)  | ⅛% of chunks   | ¼ as frequent      |
| Lava Lake (underground) | 2⁷⁄₉% of chunks | ¼ as frequent  |
| Lava Source          | 5 per chunk    | ¼ as frequent      |
| Nether Fortress, Piglin Bastion | 1.3 km | 3× farther apart |
| Nether Fossil        | 64 m           | 2× farther apart   |
| Ocean Monument       | 2 km           | 4× farther apart   |
| Ocean Ruin           | 0.5 km         | 1½× farther apart  |
| Pillager Outpost     | 1 km           | 2× farther apart   |
| Ruined Portal        | 2.5 km         | 4× farther apart   |
| Shipwreck            | 1.5 km         | 4× farther apart   |
| Stronghold           | 2.5 km – 67 km from origin | 2× farther apart, extending 2¾x farther out |
| Swamp Hut            | 1 km           | 2× farther apart   |
| Trail Ruin           | 1.1 km         | 2× farther apart   |
| Trial Chambers (Minecraft 1.21+) | 1.1 km | 2× farther apart |
| Village              | 1 km           | 2× farther apart   |
| Woodland Mansion     | 2.5 km         | 2× farther apart   |


## Usage

**When creating your world,** go to the “More” tab, click on “Data
Packs”, and “Open Pack Folder”.  Copy
`WidespreadStructures-`_version_`.zip` into this folder, then it will
appear in the list of “Available” data packs.  Click the rightward
arrow on the data pack icon to select it for your world, ensuring it
appears _above_ the “Default” Minecraft data.

It’s also possible to add this data pack to an existing world.
However, doing so will only affect chunks generated _after_ the data
pack has been added, not existing chunks.
